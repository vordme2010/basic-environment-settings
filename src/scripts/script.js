const nav = document.querySelector('.header__burger-nav')
const line = document.querySelector('.header__burger-line')
document.querySelector('.header__burger').addEventListener('click', () => {
    line.classList.toggle("header__burger-line--active")
    nav.classList.toggle("header__burger-nav--active")
})